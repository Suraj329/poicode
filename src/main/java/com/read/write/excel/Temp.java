package com.read.write.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Temp {

	File file = new File(System.getProperty("user.dir") + "//Data//Book1.xlsx");
	FileInputStream input;
	XSSFWorkbook workbook;
	XSSFSheet sheet;
	XSSFRow row;
	XSSFCell cell;

//	public void writeExcel(String fileName, String sheetName, int rowNumber, int cellNumber, String typeText) throws InvalidFormatException, IOException

	public void writeExcel(String sheetName, int rowNumber, int cellNumber, String typeText) throws InvalidFormatException, IOException {

		input = new FileInputStream(file);
		workbook = new XSSFWorkbook(input);
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowNumber);
		cell = row.createCell(cellNumber);
		cell.setCellValue(typeText);
		FileOutputStream output = new FileOutputStream(file);
		workbook.write(output);
		output.close();
		workbook.close();
	}

	public void readExcel(String sheetName, int rowNumber, int cellumber) throws InvalidFormatException, IOException {
		workbook = new XSSFWorkbook(file);
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowNumber);
		cell = row.getCell(cellumber);
		System.out.println(cell.toString());
	}

	public static void main(String[] args) throws InvalidFormatException, IOException {
		Temp t = new Temp();
//		t.readExcel("Sheet1", 0, 0);
		t.writeExcel("Sheet1", 0, 0, "ASDF");
		t.writeExcel("Sheet1", 2, 5, "48");
		t.readExcel("Sheet1", 0, 1);
	}

}
