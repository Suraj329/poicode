package com.read.write.excel;

import java.io.IOException;
import java.util.ArrayList;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelManupulation {
	
	protected static XSSFWorkbook workbook;
	protected static XSSFSheet sheet;
	protected static XSSFRow row;
	protected static XSSFCell cell;
	protected ArrayList<String> UserID;
	
	public static void getExcelAllData(String fileName, String sheetName) throws IOException
	{
		workbook = new XSSFWorkbook(System.getProperty("user.dir")+"//TestData//"+fileName+".xlsx");
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(0);
		int totalRows = sheet.getLastRowNum();
		int totalCols = row.getLastCellNum();
		System.out.println(totalRows + " \t" + totalCols);
		String excelData [][] = new String [totalRows] [totalCols];
		
		for (int rows = 1; rows <= totalRows; rows++) 
		{
			for (int cols = 0; cols < totalCols; cols++) 
			{
				row = sheet.getRow(rows);
				cell = row.getCell(cols);
				excelData[rows-1][cols]=cell.toString();
			}
		}
		
		for (int i = 0; i < totalRows; i++) 
		{
			for (int j = 0; j < totalCols; j++) 
			{
					System.out.print(excelData[i][j]+"\t");	
			}
			System.out.println("");
		}	
	}

	public static String getCellExcelData(int rowNumber, int cellNumber) throws IOException
	{
		workbook = new XSSFWorkbook(System.getProperty("user.dir")+"//TestData//APIs_Data.xlsx");
		sheet = workbook.getSheet("Sheet1");
		row = sheet.getRow(rowNumber);
		cell = row.getCell(cellNumber);
		return cell.toString();
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
