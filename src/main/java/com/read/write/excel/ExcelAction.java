package com.read.write.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelAction {
	
	private File file = new File(System.getProperty("user.dir")+"//Data//Book1.xlsx");
	private XSSFWorkbook workbook;
	private XSSFSheet sheet;
	private XSSFRow row;
	private XSSFCell cell;
	private FileInputStream input;
	private FileOutputStream output;
	
	
	public String readExcel(String sheetName, int rowNumber, int cellNumber) throws IOException, InvalidFormatException
	{
		
		workbook = new XSSFWorkbook(input);
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowNumber);
		cell = row.getCell(cellNumber);
		return cell.toString();
	}
	
	public void writeExcel(String sheetName, int rowNumber, int cellNumber, String typeText) throws IOException, InvalidFormatException
	{
		input = new FileInputStream(file);
		workbook = new XSSFWorkbook(input);
		sheet = workbook.getSheet(sheetName);
		row = sheet.getRow(rowNumber);
		cell = row.createCell(cellNumber);
		cell.setCellValue(typeText);
		
		output = new FileOutputStream(file);
		workbook.write(output);
		output.close();
		workbook.close();
		input.close();
	}
}
